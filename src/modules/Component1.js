import React, { useState } from 'react'

function Component1() {
	const [numar , schimbNR ] = useState(1)
	
	function adaog() {
		schimbNR(numar + 1)
	}
	
	return (
		<>
			<h1>Component1</h1> 
			<p>{numar}</p>
			<button onClick={adaog}>Click</button>
		</>
		
	);
	
}

export default Component1;
			
